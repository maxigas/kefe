#!/usr/bin/python3
# Anti-platform scraper
# kefe.py

from requests import get, post
import re, sys, os

if len(sys.argv) == 2:
    try:
        url = sys.argv[1] if sys.argv[1][-1] == '/' else sys.argv[1] + '/'
        profile = url.split('/')[-2]
        os.makedirs(profile, exist_ok=True)
    except:
        print("Could not parse argument(s).")
        exit(0)
else:
    print("Usage: ./kefe.py [https://instagram.com/PROFILENAME]")
    exit(0)

def save():
    i = 0
    def saver(url, filename=False):
        nonlocal i
        i += 1
        if not filename: filename = url.split('/')[-1]
        print("Saved:", i, "/", total, ":", filename)
        with open(profile + '/' + filename, 'wb') as f:
            f.write(get(url).content)
    return saver

jpgfp = re.compile('https:.*?akamaihd.net..hphotos-.*?.jpg')
urls = [ u.replace('\\','') for u in jpgfp.findall(get(url).text) if not "s640x640" in u ]
total = len(urls)
print("Found", total, "images on Instagram:", url)
f = save()
f(urls[0], '000_profile_image.jpg')
list(map(f, urls[1:]))
print("Saved", total, "images to:", profile)
